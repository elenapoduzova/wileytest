# WileyTest
Test project for Wiley.

Written in Java in Eclipse IDE.

Using Selenium WebDriver to interact with browser, Rest-Assured to work with REST, TestNG as testing framework, Allure to make reports and Gradle to build project.
Test suites are started from testng.xml.

Works with jdk11.


## Part1. Java nd Selenium WebDriver.
UI test stack:

*  Java
*  SeleniumWebDriver
*  TestNG
*  Allure
*  Gradle

Using Page Object and Element Object patterns.

## Part2. API.
API test stack:

*  Java
*  Rest-Assured
*  TestNG
*  Allure
*  Gradle

Using JsonSchema to validate response structure and POJO to check information in response.

## How to start

Install jdk11 and Gradle.
Clone repository.
Tests can be run from IDE or by executing shell script start_tests.bat.
After all tests are completed, Allure report will be automatically opened in browser.