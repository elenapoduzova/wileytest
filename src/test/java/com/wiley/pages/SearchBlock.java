/** 
 * Класс для работы с блоком поиска.
 * @autor Елена Подузова
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.qameta.allure.Step;

public class SearchBlock extends AbstractElement  {

	private By searchField = By.id("js-site-search-input");
	private By suggestions = By.id("ui-id-2");
	
	public SearchBlock(WebDriver driver, By selector) {
		super(driver, selector);
		// TODO Auto-generated constructor stub
	}
	
	@Step("Enter search text and check suggestion")
	public void EnterTextAndCheckSuggestions(String text){
		_element.clear();
		_element.sendKeys(text);
		
		WebElement suggestionsElement = driver.findElement(suggestions);
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.visibilityOf(suggestionsElement));
			
		List<WebElement> suggestedElements = driver.findElements(By.cssSelector("section.searchresults-section.search-related-content.suggestions > div > div.searchresults-item ui-menu-item"));
		List<WebElement> productElements = driver.findElements(By.cssSelector("section.searchresults-section related-content-products-section > div > div.searchresults-item ui-menu-item"));
		
		suggestedElements.forEach(element->{
			Assert.assertTrue(element.getText().contains(text));
		});
		
		productElements.forEach(element->{
			Assert.assertTrue(element.getText().contains(text));
		});
	}
	
	@Step("Enter search text and press submit button")
	public void EnterTextAndConfirm(String text){
		_element.clear();
		_element.sendKeys(text);
		
		driver.findElement(By.cssSelector("#main-header-container > div > div.main-navigation-search > div > form > div > span > button")).click();
	}

}