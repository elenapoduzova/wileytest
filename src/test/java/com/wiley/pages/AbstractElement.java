/** 
 * Класс с базовой реализацией работы со элементом.
 * @autor Елена Подузова
 * com.wiley.pagesва
 * @version 1.0
*/

package com.wiley.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wiley.Base;

import io.qameta.allure.Step;

public class AbstractElement  extends Base {
	
	protected WebDriver driver;
	protected WebElement _element;
	
	public AbstractElement(WebDriver driver, By selector) {
		this.driver = driver;
		_element = driver.findElement(selector);
	}
	
	@Step("Check if element presents")
	public boolean IsPresents()
	{
		return _element.isDisplayed();
	}
	
	@Step("Wait element to appear")
	public void WaitToAppear(){
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.visibilityOf(_element));
	}
	
	@Step("Wait until element is hidden")
	public void WaitToHide(){
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.invisibilityOf(_element));
	}
	
	@Step ("Get element count")
	public int GetElementsCount(By by){
		return _element.findElements(by).size();
	}

	@Step("Focus on element")
	public void Focus(){
		_element.click();
	}
}