/** 
 * Класс для работы со станицей education.
 * @autor Елена Подузова
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.qameta.allure.Step;

public class EducationPage extends AbstractPage {
	
	private String pagePath = "/en-us/Education-c-ED00";
		
	public EducationPage(WebDriver driver){
		super(driver);
	}
	
	public void Open(){
		OpenPath(pagePath);
	}
	
	@Step("Wait 'education' page is loaded")
	public void WaitPageIsLoaded(){
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.urlContains(pagePath));
		
		ContentDialog dialog = new ContentDialog(driver, By.className("modal-dialog"));
		if (dialog.IsPresents()){
			dialog.Dismiss();
		}
		
		Assert.assertTrue(driver.getTitle().equals("Education | Subjects | Wiley"));
		Assert.assertTrue(driver.findElement(By.cssSelector("li.active")).getText().equals("Education"));
	}
	
	@Step("Check 'subject' elements on page")
	public void CheckSubjects(ArrayList<String> titlesText){
		WebElement sidePanel = driver.findElement(By.cssSelector("div.side-panel"));
		List<WebElement> subjects = sidePanel.findElements(By.cssSelector("li > a"));
	
		subjects.forEach(element->{
			Assert.assertEquals(element.getText(), titlesText.get(subjects.indexOf(element)));
		});
	}
}