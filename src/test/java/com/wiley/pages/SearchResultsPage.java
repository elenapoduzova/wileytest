/** 
 * Класс для работы со страницей результатов поиска.
 * @autor Елена Подузова
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.qameta.allure.Step;

public class SearchResultsPage extends AbstractPage {
	
	private String pagePath = "/en-us/search?pq=%s%7Crelevance";
		
	public SearchResultsPage(WebDriver driver) {
		super(driver);
	}
	
	public void Open(String param){
		OpenPath(String.format(pagePath, param));
	}
	
	@Step("Wait search page is ready")
	public void WaitToReady(){
		waitForElementAttachedToAPage(By.className("search-result-tabs-wrapper"));
		
		ContentDialog dialog = new ContentDialog(driver, By.className("modal-dialog"));
		if (dialog.IsPresents()){
			dialog.Dismiss();
		}
	}
	
	@Step("Check search results")
	public void CheckResultsElements(String searchText){
		
		List<WebElement> productElements = driver.findElements(By.cssSelector("section.product-item"));
		
		Assert.assertEquals(productElements.size(), 10);
		
		productElements.forEach(element->{
			Assert.assertTrue(element.findElement(By.cssSelector("h3.product-title")).getText().contains(searchText));
			if (element.findElement(By.cssSelector("div.productButtonGroupName")).getText().equals("E-Book")){
				Assert.assertTrue(element.findElements(By.cssSelector("button.small-button add-to-cart-button js-add-to-cart")) != null);	
			}
			else if (element.findElement(By.cssSelector("div.productButtonGroupName")).getText().equals("E-Book")){
				Assert.assertTrue(element.findElements(By.cssSelector("a.small-button learn-more-button")) != null);
			}
		});
	}
	
}
