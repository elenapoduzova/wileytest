/** 
 * Класс с базовой реализацией работы со страницей.
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.pages;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wiley.Base;
import com.wiley.tests.BaseTest;

import io.qameta.allure.Step;

public class AbstractPage extends Base {
	
	WebDriver driver;
	
	public AbstractPage(WebDriver driver) {
		this.driver = driver;
	}
	
	@Step("Open page")
	public void OpenPath(String pagePath) {
		driver.get(BaseTest.GetBaseUrl() + pagePath);
	}
	
	@Step("Open main page")
	public void OpenMain() {
		driver.get(BaseTest.GetBaseUrl());
	}
		
	public void Reload() {
		driver.navigate().refresh();
	}
		
	/**
     * Дождаться появления элемента на странице. Элемент определен селектором.
     * Что элемент появился, определяем по тому, что он стал видимым.
     * @param selector - селектор элемента.
    */	
	@Step ("Wait for element is presented in a page")
	public void waitForElementPresense(WebElement element) {
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.visibilityOf(element));
	}	

	@Step ("Wait for element is not in a page")
	public void waitForElementAttachedToAPage(By selector) {
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.numberOfElementsToBeMoreThan(selector, 0));
	}
	
	/**
     * Дождаться исчезновения элемента на странице. Элемент определен селектором.
     * Что элемент исчез, определяем по тому, что он стал невидимым.
     * @param selector - селектор элемента.
    */	
	@Step ("Wait for element is not in a page")
	public void waitForElementNotPresented(By selector) {
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.numberOfElementsToBe(selector, 0));
	}
	
}
