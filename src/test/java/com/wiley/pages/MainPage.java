/** 
 * Класс для работы с главной страницей.
 * @autor Елена Подузова
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import io.qameta.allure.Step;

public class MainPage extends AbstractPage {
	
	private String pagePath = "/en-us";
		
	public MainPage(WebDriver driver) {
		super(driver);
	}
	
	public void Open(){
		OpenPath(pagePath);
		
		ContentDialog dialog = new ContentDialog(driver, By.className("modal-dialog"));
		if (dialog.IsPresents()){
			dialog.Dismiss();
		}
	}
	
	@Step("Set mouse over header element")
	public void MouseOverHeaderElement(int tabNumber)
	{
		WebElement menu = driver.findElement(By.xpath("//nav[@role='navigation']/ul/li["+tabNumber+"]/a"));
		Actions action = new Actions(driver);
		action.moveToElement(menu).perform();
	}
}
