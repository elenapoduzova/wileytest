/** 
 * Класс для работы с поп-апом с предложением смены языка.
 * @autor Елена Подузова
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.qameta.allure.Step;

public class ContentDialog extends AbstractElement  {

	By closeButton = By.cssSelector("#country-location-form > div.modal-header > button");
	
	public ContentDialog(WebDriver driver, By selector) {
		super(driver, selector);
	}
	
	@Step("Close dialog")
	public void Dismiss()
	{
		driver.findElement(closeButton).click();
		WaitToHide();
		sleep(driver, 5);
	}
}
