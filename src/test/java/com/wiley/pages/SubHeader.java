/** 
 * Класс для работы с выпадающим меню.
 * @autor Елена Поузова
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.qameta.allure.Step;

public class SubHeader extends AbstractElement  {
	
	//#Level1NavNode1 > ul > li:nth-child(1)

	public SubHeader(WebDriver driver, By selector) {
		super(driver, selector);
		WaitToAppear();
	}
	
	@Step("Click element in submenu")
	public void ClickElement(int number){
		WebElement clickObject = _element.findElement(By.cssSelector(".dropdown-submenu:nth-child("+number+") > a"));
		
		new WebDriverWait(driver, DRIVERWAIT)
			.until(ExpectedConditions.elementToBeClickable(clickObject));
		
		clickObject.click();
	}
	
	@Step("Check subheader items count")
	public void CheckSubHeaderItemsCount(int needCount){
		int count = GetElementsCount(By.cssSelector("ul > li"));
		Assert.assertEquals(count, needCount);
	}
	
	public void CheckSubHeaderElements(ArrayList<String> titlesText){
		List<WebElement> elementsList = _element.findElements(By.cssSelector("ul > li"));
		
		elementsList.forEach(element->{
			Assert.assertEquals(element.getText(), titlesText.get(elementsList.indexOf(element)));
		});
	}

}
