package com.wiley.tests;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
	
	String code;
	String name;
	String url;
	List<BookImage> images;
	
	public Product() {
	}
	    
    public String code() {
    	return code;
    }
    
    public String getName() {
    	return name;
    }
    
    public String getUrl() {
    	return url;
    }
    
    public List<BookImage> getImages() {
    	return images;
    }
}