/** 
 * Класс с базовыми функциями для работы с rest-assured.
 * Так же вынесены часто использующиеся спецификации (параметры запроса или ответа).
 * @autor Елена Подузова
 * @version 1.0
*/

package com.wiley.tests;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

import java.nio.file.Files;
import java.nio.file.Paths;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import org.json.JSONObject;

public class ApiBase {
	/**
	 * Задать baseUrl
	 * @param url
	 */
	protected void SetBaseUrl(String url)
	{
		RestAssured.baseURI = url;
	}
	
	/**
     * Создать спецификацию запроса.
     * @param contentType. 
    */	
	protected RequestSpecBuilder CreateRequestSpec(ContentType contentType) {
		return new RequestSpecBuilder()
				.setContentType(contentType)
			    .setAccept(ContentType.JSON);
	}
	
	/**
	 * Отправка запроса c параметрами в path с помощью RestAssured и валидация полученого ответа 
	 * @param requestSpec
	 * @param method
	 * @param endPoint
	 * @param queryParams
	 * @param jsonScheme
	 * @throws Exception
	 */
	public void AssertRequest(Method method, String endPoint, Object[] pathParams, String jsonScheme) throws Exception {
		RequestSpecification spec = CreateRequestSpec(ContentType.JSON).build();
		RestAssured.given(spec).request(method, endPoint, pathParams).then().assertThat().body(matchesJsonSchema(jsonScheme));
	}
	
	public Response ExecuteRequest(Method method, String endPoint, Object[] pathParams) throws Exception {
		RequestSpecification spec = CreateRequestSpec(ContentType.JSON).build();
		return RestAssured.given(spec).request(method, endPoint, pathParams);
	}
	
	/**
	 * Прочитать json схему из файла
	 * @param fileName
	 * @param schemeName
	 * @return
	 * @throws Exception
	 */
	public String GetJsonScheme(String fileName, String schemeName) throws Exception {
		String dataString = new String(Files.readAllBytes(Paths.get("src","test","resources", fileName+".json").toAbsolutePath()));
        Object jsonObject = new JSONObject(dataString).get(schemeName);
        return jsonObject.toString();
	}
}
