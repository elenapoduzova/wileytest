/**
 * Реализация API теста для wiley.com
 * 1. Check that:  
	a. GET method for https://www.wiley.com/en-us/search/autocomplete/comp_00001H9J?term=Java  
	Returns response with at least these parts: 
	- 4 suggestions contain attribute “term” : value starting with the preformatted
	highlighted word java inside like <span class=\"search-highlight\">java</span> 
	- 4 products contain attribute “name”: value includes the preformatted highlighted word
	Java inside like  "<span class='search-highlight'>Java</span>" 
	- 4 pages with attribute “title”: value includes word Wiley 
	b. Get from previous response any image url from products -&gt; images  
	Make GET request and check if image has got width 300 px 
	2. There is a simple HTTP Request &amp; Response Service https://httpbin.org  
	https://httpbin.org/#/Dynamic_data/post_delay__delay_ 
	 
	POST/delay/{delay}  
	Returns a delayed response (max of 10 seconds).
*/

package com.wiley.tests;

import io.restassured.http.Method;
import io.restassured.response.Response;

import java.util.List;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WileyAPITest extends ApiBase {
 
	@BeforeClass
    public void apiTestSetup() {

    }
    
	private String GetJsonScheme(String jsonField) throws Exception{
		return GetJsonScheme("WileyValidate", jsonField);
	} 
	
	@Test(priority = 1, groups = "session api", description = "Session Update Token")
	public void CheckGetMethodWithSchema() throws Exception {
		SetBaseUrl("https://www.wiley.com");
		AssertRequest(Method.GET, EndPoints.getInfo, new Object[] {"Java"}, GetJsonScheme("getInfo"));
	}
	
	@Test(priority = 2, groups = "session api", description = "Session Update Token")
	public void CheckGetMethodWithJson() throws Exception {
		SetBaseUrl("https://www.wiley.com");
		Response response = ExecuteRequest(Method.GET, EndPoints.getInfo, new Object[] {"Java"});
		
		List<Suggestion> suggestions = response.getBody().jsonPath().getList("suggestions", Suggestion.class);
		
		Assert.assertTrue(suggestions.size() >= 4);
		suggestions.forEach(element->{
			Assert.assertTrue(element.term.contains("<span class=\"search-highlight\">java</span>"));
		});
		
		List<Product> products = response.getBody().jsonPath().getList("products", Product.class);
		
		Assert.assertTrue(products.size() >= 4);
		products.forEach(element->{
			Assert.assertTrue(element.name.contains("<span class='search-highlight'>Java</span>"));
			
			List<BookImage> images = element.images;
			images.forEach(image->{
				URL url;
				try {
					url = new URL(image.url);
					BufferedImage imageRes = ImageIO.read(url);
					Assert.assertEquals(imageRes.getWidth(), 300);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		});

		
		List<Page> pages = response.getBody().jsonPath().getList("pages", Page.class);
		
		Assert.assertTrue(pages.size() >= 4);
		pages.forEach(element->{
			Assert.assertTrue(element.title.contains("Wiley"));
		});
	}
	
	@Test(priority = 3, groups = "session api", description = "Session Update Token")
	public void CheckResponseTime() throws Exception {
		SetBaseUrl("https://httpbin.org");
		Response response = ExecuteRequest(Method.POST, EndPoints.service, new Object[] {"10"});
		System.out.println("Response Time : " + response.getTime());
		Assert.assertTrue(response.getTime() <= 10000);
	}
}
