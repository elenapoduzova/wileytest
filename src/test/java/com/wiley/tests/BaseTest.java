/** 
 * Класс с базовой реализацией работы с тестом Selenium.
 * @autor Елена Подузова
 * com.wiley.pages
 * @version 1.0
*/

package com.wiley.tests;

import com.wiley.Base;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest extends Base {
	
	protected WebDriver driver;
	protected static String baseUrl;
	
	@BeforeTest
	public void beforeTest() {
		WebDriverManager.chromedriver().setup();
	}

	@Parameters({ "url" })
    @BeforeClass
    public void beforeClass(String url) throws Exception {
		baseUrl = url;
		
		CreateResultsDir("InterfaceTest");
		
    	ChromeOptions options = new ChromeOptions();
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		options.setExperimentalOption("prefs", prefs);
		
        driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.manage().window().maximize();
    }
    
    @AfterTest
    public void afterClass() {
        driver.quit();
    }
    
	@AfterMethod
	public void testCaseFailure(ITestResult testResult) throws Exception { 
		actionOnFailure(driver, testResult.getStatus(), testResult.getName());
	}
	
	public static String GetBaseUrl() {
		return baseUrl;
	}
	
}
