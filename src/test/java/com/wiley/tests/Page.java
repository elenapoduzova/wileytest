package com.wiley.tests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Page {
	
	String id;
	String boost;
	String title;
	String url;
	String content;
	
	public Page() {
	}
	    
    public String getId() {
    	return id;
    }
    
    public String getTitle() {
    	return title;
    }
    
    public String getUrl() {
    	return url;
    }
    
    public String getContent() {
    	return content;
    }
}