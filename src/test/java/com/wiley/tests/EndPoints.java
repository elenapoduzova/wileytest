/** 
 * Класс с переменными, содержащими end-point для тестирования запросов.
 * @version 1.0
*/

package com.wiley.tests;

//Here are all end-points for restAPI testing
public final class EndPoints {
		
	public static final String getInfo = "/en-us/search/autocomplete/comp_00001H9J?term={term}";
	public static final String service = "/#/Dynamic_data/post_delay__{delay}_";
    
}