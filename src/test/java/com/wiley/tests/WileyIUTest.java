/**
 * Реализация UI теста для wiley.com
 * The main page to start with: https://www.wiley.com/en-us  
	1. Check items under Who We Serve for sub-header  
	·                     There are 11 items under resources sub-header  
	·                     Titles are “Students”, “Instructors”, “Book Authors”, “Professionals”, “Researchers”,
	“Institutions”, “Librarians”, “Corporations”, “Societies”, “Journal Editors”, “Government”  
	 
	2. Check search functionality:  
	Enter “Java” in the search input and do not press the search button (with magnifying glass icon) 
	·                     Area with related content is displayed right under the search header  
	 
	3. Enter “Java” in the search input and press the search button  
	·                     Only titles containing “Java” are displayed  
	·                     There are 10 titles on the page 
	·                     Each title has at least one “Add to Cart” button for E-Book/Print version and “VIEW ON
	WILEY ONLINE LIBRARY” for O-BOOK version 
	 
	4. Go to “Subjects” top menu, select “Education”  
	·                     Check “Education” header is displayed  
	·                     13 items are displayed under “Subjects” on the left side of the screen and the texts are:
		"Information & Library Science", "Education & Public Policy", "K-12 General", "Higher Education General", 
		"Vocational Technology", "Conflict Resolution & Mediation (School settings)", "Curriculum Tools- General",
		"Special Educational Needs", "Theory of Education", "Education Special Topics", "Educational Research & Statistics", 
		"Literacy & Reading", "Classroom Management"
 */

package com.wiley.tests;
import com.wiley.pages.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class WileyIUTest extends BaseTest {
 
	private MainPage mainPage;
	
	/**
     * Открыть главную страницу и закрыть диалог с предложением перейти на другой язык
    */
	@Test (priority=1, description= "Open main page and close location dialog")
	public void OpenMainPage() {
		mainPage = new MainPage(driver);
		mainPage.Open();
	}
	
	@Test (priority=2, description= "Open subtitle and check elements")
	public void OpenSubtitleServeDropdown(){
		mainPage.MouseOverHeaderElement(1);
		SubHeader dropDown = new SubHeader(driver, By.cssSelector("#Level1NavNode1"));
		dropDown.CheckSubHeaderItemsCount(12);
		
		ArrayList<String> titlesList = new ArrayList<String>( 
	            Arrays.asList("Students", 
	                          "Instructors", 
	                          "Book Authors", 
	                          "Professionals", 
	                          "Researchers", 
	                          "Institutions", 
	                          "Librarians", 
	                          "Corporations", 
	                          "Societies", 
	                          "Journal Editors", 
	                          "Bookstores", 
	                          "Government"));
		dropDown.CheckSubHeaderElements(titlesList);
	}
	
	@Test (priority=3, description= "Check search")
	public void CheckSearch(){
		SearchBlock search = new SearchBlock(driver, By.id("js-site-search-input"));
		search.Focus();
		search.EnterTextAndCheckSuggestions("Java");
	}
	
	@Test (priority=4, description= "Check search results page")
	public void CheckSearchResults(){
		SearchBlock search = new SearchBlock(driver, By.id("js-site-search-input"));
		search.Focus();
		search.EnterTextAndConfirm("Java");
		SearchResultsPage page = new SearchResultsPage(driver);
		page.WaitToReady();
		page.CheckResultsElements("Java");
	}
	
	@Test (priority=5, description= "Check education page")
	public void CheckEducationPage(){
		mainPage.Open();

		mainPage.MouseOverHeaderElement(2);
		SubHeader dropDown = new SubHeader(driver, By.cssSelector("#Level1NavNode2"));
		dropDown.ClickElement(9);
		EducationPage page = new EducationPage(driver);
		page.WaitPageIsLoaded();
		
		ArrayList<String> titlesList = new ArrayList<String>( 
	            Arrays.asList("Information & Library Science", 
	                          "Education & Public Policy", 
	                          "K-12 General", 
	                          "Higher Education General", 
	                          "Vocational Technology", 
	                          "Conflict Resolution & Mediation (School settings)", 
	                          "Curriculum Tools- General", 
	                          "Special Educational Needs", 
	                          "Theory of Education", 
	                          "Education Special Topics", 
	                          "Educational Research & Statistics", 
	                          "Literacy & Reading",
	                          "Classroom Management"));
		page.CheckSubjects(titlesList);
	}
}
