package com.wiley.tests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Suggestion {
	
	String term;
	
	public Suggestion() {
	}
	    
    public String getTerm() {
    	return term;
    }
}