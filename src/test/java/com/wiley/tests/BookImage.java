package com.wiley.tests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BookImage {
	
	String imageType;
	String format;
	String url;
	
	public BookImage() {
	}
	    
    public String getImageType() {
    	return imageType;
    }
    
    public String getFormat() {
    	return format;
    }
    
    public String getUrl() {
    	return url;
    }
    
}