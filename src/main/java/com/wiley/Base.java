/** 
 * Класс с базовыми функциями для работы с webdriver.
 * @autor Елена П�com.wileyа
 * @version 1.0
*/

package com.wiley;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.Calendar; 
import java.util.Date;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.apache.commons.io.FileUtils;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class Base {

	/** Стандартное время ожидания для WebDriver */
	protected int DRIVERWAIT = 5;
	
	/** Текущее время для генерации папки для отчета, скриншотов etc */
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd_HH-mm-ss");
	private String screenShotPath;
	private String reportPath;

	/**
     * Переключиться на алерт и закрыть его.
     *  
    */	
	public void safeAlertDissmiss(WebDriver driver) {
	    try {
	         driver.switchTo().alert().dismiss();
	    } catch (NoAlertPresentException e) {
	         //doing nothing, because there is no alert
	    }
	}

	/**
     * �?зменить размер окна браузера на указанный.
     * @param height - высота.
     * @param width - ширина. 
    */	
	public void setWindowSize(WebDriver driver, int height, int width) {
        Dimension dimension = new Dimension(height,width);
        driver.manage().window().setSize(dimension);
	}
	
	/**
     * Создать папку, куда будут складываться скриншоты и отчеты.
     * Структура: AutotestResults/год/месяц/день_и_время.
     *  
    */	
	public void CreateResultsDir(String rootFolder) {
		//create month folder if not exist
		String path = rootFolder+"//"+Calendar.getInstance().get(Calendar.YEAR)+"//"+ Month.of(Calendar.getInstance().get(Calendar.MONTH));
		reportPath = path+"//"+dateFormat.format(new Date());
		screenShotPath = path+"//"+dateFormat.format(new Date())+"//Screenshots";
		File mainDir = new File(rootFolder);
		if (!mainDir.exists()) { 
			mainDir.mkdir();
        }
		File monthDir = new File(path);
		if (!monthDir.exists()) { 
			monthDir.mkdir();
        }
		//create current date folder
		new File(reportPath).mkdirs();
		//create screenshots folder
		new File(screenShotPath).mkdirs();
	}

	/**
     * Запускает ожидание WebDriver по таймауту.
     * @param sleepTime - время в секундах.
    */	
	public void sleep(WebDriver driver, int sleepTime) {
		try {
			Thread.sleep(sleepTime*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
     * Сделать скриншот текущего состояния браузера и сохранить в папку.
     * @see Base#CreateResultsDir
     * @param fileName - название файла.
    */
    public String TakeScreenshot(WebDriver driver, String fileName) throws Exception {
        try {
	        TakesScreenshot scrShot =((TakesScreenshot)driver);
	
	        //Call getScreenshotAs method to create image file
	        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	
	        //Move image file to new destination
	        String imagePath = screenShotPath+"//"+fileName+".jpg";
	        File DestFile=new File(imagePath);
	
	        //Copy file at destination
	        FileUtils.copyFile(SrcFile, DestFile);
	        
	        return imagePath;
        } catch(Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

	/**
     * Обработать ситуацию с отрицательным результатом выполнения тесткейса.
     * Если тест провален, то делаем скриншот всей страницы, код страницы, сохраняем логи консоли и network из браузера
     * и добавляем все это в отчету.
     * @param testResult - результат тесткейса.
     * @param testName - название тесткейса.
    */
	public void actionOnFailure(WebDriver driver, int testResult, String testName) throws Exception { 
		if (testResult == 2) { 
			takeScreenshotToReport(driver, testName);
			GetConsoleLogs(driver);
			getPageSource(driver);
		} 
	}
	
	/**
     * Обертка для {@link Base#TakeAreaScreenshot}.
     * Формирует имя скриншота для отчета.
     * @param testName - название тесткейса.
    */    
	public void takeScreenshotToReport(WebDriver driver, String testName) throws Exception { 
		addScreenshotToReport(TakeScreenshot(driver, testName+"_fail"));
	}

	/**
     * Получить код страницы.
    */  
	@Attachment (value = "Page sorce")
	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	/**
     * Получить логи из консоли браузера.
    */  
    @Attachment
    public StringBuilder GetConsoleLogs(WebDriver driver) {
    	StringBuilder logs = new StringBuilder();
    	logs.append("***** Browser Console Logs: *****\n");
        LogEntries browserLogEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : browserLogEntries) {
        	String entryStr = new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage();
            //do something useful with the data
        	logs.append( entryStr +"\n");
        }
        /*logs.append("\n\n*******Perfomance Logs: *********\n");
        LogEntries networkLogsEntries = driver.manage().logs().get(LogType.PERFORMANCE);
        for (LogEntry entry : networkLogsEntries) {
            //do something useful with the data
        	logs.append( entry.toString() +"\n");
        }
        logs.append("*********************************\n");*/
    	return logs;
    }
    
	/**
     * Приложить скриншот к отчету allure.
     * @param screenshotPath - путь к скришоту.
    */  
	@Attachment (value = "Page screenshot", type = "image/jpg")
	public static byte[] addScreenshotToReport(String screenshotPath) throws IOException {
	     return Files.readAllBytes(Paths.get(screenshotPath));
	}
}
